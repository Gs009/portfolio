<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'portfolio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GjAu`/& TeRq4?zAp/wrER;6V$C/#m8i2#7.ndR!MX$HB(AiG/%v&R>|_D!A<!Xj');
define('SECURE_AUTH_KEY',  'sEjL`NE}cyv3;X~_/k+:rfI!DREXqV0hJrh-yxUv3JP[j^~{dpo0pJ1$)GzY-,iD');
define('LOGGED_IN_KEY',    '_&u0lw1bnuX:KImMh*$k~IdEQLIGEdSrSV gKU)X1rVF|e}J{A+:MmyeMMHT3iX}');
define('NONCE_KEY',        'RaMS)0e{|`AQPCTXHaLaTz%5P`xWw3P&4*U9T/L8jMd)yF?PF;1I_,b052la^U$u');
define('AUTH_SALT',        '],=)RSTb,}g5s/75UZcbJY6v%g>/`/{l9rMn5:TF[-L3d>[BT<C|tNyeBgu,#mgu');
define('SECURE_AUTH_SALT', '*Xw&OTb*k)7)p(!OSLVS14cN+3!a.tN`Q#i.fP@t~U}m#a]KZj :hEcO0Cte^83_');
define('LOGGED_IN_SALT',   'dmP& ,=wMgvoI#|z]vLK*-}[3R0oX /V/TY#}>hkH#aS#rY3HyM6}qNo<qTOUl$H');
define('NONCE_SALT',       '>-2[vP@,2xaEehWmswt}zv4N>2%j$vF)A=E%V=kq;k9e/3bb`C&.ba5@a}oa}7{#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
